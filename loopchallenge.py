#!/usr/bin/env python3

'''Kent Kassube
   This is my challenge sumission for using lists with sublists, how to use splicing to select specific information
   and how to use a for loop to iterate through the list.'''

list = [["car", 40000, "jeep gladiator"], ["house", 250000, "pine glen 123 main street"],[ "boat", 50000, "speed"],  ["castle", 300000000, "camelot"]]   # create a list with sublets called, "list"


def main():
    '''exploring lists and sublists using splice and for loops'''

    # Print the first item in each list:
    print("This is my first asset, with value, and description of:", list[0][0], list[1][1], list[2][2], "\n")
    
    # Iterate through the list using a for loop:
    for asset in list:
        print(asset[0],":", "is valued at:", asset[1],",", "with a description of:", asset[2])

# best practice technique to call our python script
if __name__ == "__main__":
    main()     # calls the "main" function to run

